import React from 'react';
import { connect } from 'react-redux';
import { Container, Row } from 'react-bootstrap';

import Product from '../produit-component';
import PriceComponent from '../price-component';
import NouveauProduitComponent from '../nouveau-produit-component';
import GestionnaireComponent from '../gestionnaire-component';

const Home = ({ data }) => (
  <Container className="bg-primary" style={{ height: '100%' }}>
    <PriceComponent />
    <GestionnaireComponent />
    <Row>
      {data
        .filter((item) => item.unlock)
        .map((item) => <Product key="test" item={item} />)}
    </Row>
    <NouveauProduitComponent />
  </Container>
);

const mapToProps = (state) => ({
  data: state.data
});

export default connect(mapToProps)(Home);
