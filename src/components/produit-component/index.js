import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Row,
  Col,
  Alert,
  ProgressBar
} from 'react-bootstrap';

import store from '../../store';
import { addMoney, removeMoney } from '../price-component/actions';

import './index.scss';

class Product extends Component {
  constructor(props) {
    super(props);

    const { item } = this.props;

    this.state = {
      item,
      totalCostBuy: 0
    };

    this.handleBuyProduct = this.handleBuyProduct.bind(this);
    this.handleUpdateProduct = this.handleUpdateProduct.bind(this);

    this.startManager();
  }

  /**
   * Buy Product
   */
  buyProduct() {
    const { dispatch } = store;
    const { totalCostBuy, item } = this.state;
    const { price } = item;

    dispatch(addMoney(price));

    this.setState({
      totalCostBuy: totalCostBuy + price
    });
  }

  startManager() {
    const { item } = this.state;
    const { timeFactory, unlockManager } = item;

    if (unlockManager) {
      setInterval(() => {
        this.buyProduct();
      }, timeFactory);
    }
  }

  /**
   * handle buy product
   */
  handleBuyProduct() {
    this.buyProduct();
  }

  /**
   * handle update product
   */
  handleUpdateProduct() {
    const { dispatch } = store;
    const { item } = this.state;
    const { totalCostFactoryUnit } = item;
    const { money } = this.props;

    if (money - totalCostFactoryUnit < 0) {
      return;
    }

    dispatch(removeMoney(totalCostFactoryUnit));

    item.multi += 1;
    item.totalCostFactoryUnit *= item.multi;
    item.price += 1;

    this.setState({ item });
  }

  /**
   * render
   */
  render() {
    const {
      item,
      totalCostBuy
    } = this.state;

    const {
      image,
      multi,
      price,
      totalCostFactoryUnit,
      unlockManager
    } = item;

    const { money } = this.props;
    const percentageNextUpdate = ((money * 100) / totalCostFactoryUnit).toFixed(0);
    const colorManager = (unlockManager) ? 'bg-danger' : 'bg-info';

    return (
      <Col md={6}>
        <Row>
          <Col md={4}>
            <Row>
              <Col md={12} onClick={this.handleBuyProduct}>
                <img
                  src={image}
                  className={`shadow p-4 ${colorManager} rounded-circle img-fluid`}
                  alt="product"
                />
              </Col>
              <Col md={12}>
                <h2 className="text-center text-white">{`${price.toFixed(0)} $`}</h2>
              </Col>
            </Row>
          </Col>
          <Col md={8}>
            <Row>
              <Col md={12}>
                <Alert variant="primary">
                  <h1 className="display-6">{`$${totalCostBuy.toFixed(2)}`}</h1>
                </Alert>
              </Col>
            </Row>
            <Row className="mb-3">
              <Col md={12}>
                <ProgressBar variant="info" now={percentageNextUpdate} />
              </Col>
            </Row>
            <Row>
              <Col md={12} onClick={this.handleUpdateProduct}>
                <Alert variant="warning">
                  <h1 className="display-6">{`X${multi} : $${totalCostFactoryUnit.toFixed(2)}`}</h1>
                </Alert>
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>
    );
  }
}

const mapToProps = (state) => ({
  money: state.barrePrice.money
});

export default connect(mapToProps)(Product);
